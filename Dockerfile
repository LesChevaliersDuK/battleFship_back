FROM fsharp:4.0.1.1

WORKDIR /srv/battlefship-back

# install dependencies
RUN nuget install Suave

# COPY script/build.sh script/build.sh # @TODO: build

# copy the sources
COPY src src

# build
# RUN ./script/build.sh # @TODO: build

# clean things only needed to build (to have a lighter / faster image)
# RUN rm -fr script src # @TODO: build

EXPOSE 8083

# CMD mono ./build/server.exe # @TODO: build
CMD fsharpi src/main.fsx
