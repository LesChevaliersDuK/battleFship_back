#!/bin/sh
TARGET="build/server.exe"
SOURCE="src/main.fsx"

cd $(dirname $(dirname $0))
mkdir -p ./build
fsharpc \
	--target:exe \
	--out:$TARGET \
	--reference:"$(pwd)/Suave.1.1.3/lib/net40/Suave.dll" \
	$SOURCE
chmod +x $TARGET
