#!/usr/bin/env fsharpi

#if INTERACTIVE
#r "../Suave.1.1.3/lib/net40/Suave.dll"
#endif

open Suave
open Suave.Web
open System.Net


let config =
    { defaultConfig with
        bindings = [HttpBinding.mk HTTP (IPAddress.Parse "0.0.0.0") 8083us]
    }

startWebServer config (Successful.OK "Hello world !")
